﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace WPFStylesPlayground
{
    public class ExtentValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value is not string valueString)
            {
                return new ValidationResult(false, "value is not string");
            }

            if (valueString.Length > 0)
            {

            }

            if (!int.TryParse(valueString, out int valueInt))
            {
                return new ValidationResult(false, "value is not an integer");
            }

            const int rangeLower = 0;
            const int rangeUpper = 100;

            if (valueInt is not (>= rangeLower and <= rangeUpper))
            {
                return new ValidationResult(false, $"value is not within accepted range {rangeLower} - {rangeUpper}");
            }

            return ValidationResult.ValidResult;
        }
    }
}
