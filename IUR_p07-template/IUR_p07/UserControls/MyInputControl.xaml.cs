﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;

namespace IUR_p07
{
    /// <summary>
    /// Interaction logic for MyInputControl.xaml
    /// </summary>
    [ContentProperty(nameof(FileName))]
    public partial class MyInputControl : UserControl
    {
        public MyInputControl()
        {
            InitializeComponent();
        }

        private void myButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog()
            {
                CheckFileExists = true,
                CheckPathExists = true
            };

            if (dialog.ShowDialog() ?? false)
            {
                FileName = dialog.FileName;
            }
        }

        public string FileName
        {
            get => (string)GetValue(FileNameProperty);
            set => SetValue(FileNameProperty, value);
        }

        public static readonly DependencyProperty FileNameProperty =
            DependencyProperty.Register(nameof(FileName), typeof(string), typeof(MyInputControl));

        public event RoutedEventHandler FileNameChanged;

        public static readonly RoutedEvent FileNameChangedEvent =
            EventManager.RegisterRoutedEvent(nameof(FileNameChanged), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MyInputControl));

        private void myTextbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            e.Handled = true;
            var args = new RoutedEventArgs(FileNameChangedEvent, this);
            RaiseEvent(args);
        }
    }
}
