﻿using System.Threading;

namespace DU7.ServerOnly
{
    internal static class IDGenerator
    {
        private static ulong lastID = 0x1111111111111111;
        internal static ulong MakeID()
        {
            ulong ret, lastIDCopy;
            do
            {
                lastIDCopy = Thread.VolatileRead(ref lastID);
                ret = unchecked(lastIDCopy + 1);
            } while (lastIDCopy != Interlocked.CompareExchange(ref lastID, ret, lastIDCopy));
            return ret;
        }
    }
}
