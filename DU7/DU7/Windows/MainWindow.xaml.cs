﻿using DU7.ViewModels;
using DU7.Windows;
using System;
using System.Diagnostics;
using System.Windows;

namespace DU7
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void Window_Closed(object sender, System.EventArgs e) 
        {
            (DataContext as IDisposable)?.Dispose();
        }

        private void OpenSettings(object sender, RoutedEventArgs e)
        {
            new SettingsWindow().ShowDialog();
        }

        private void ListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Debug.WriteLine("Listbox mouse double click");
        }

        private void ListBox_Drop(object sender, DragEventArgs e)
        {

        }
    }
}
