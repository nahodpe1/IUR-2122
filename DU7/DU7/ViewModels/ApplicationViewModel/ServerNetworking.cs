﻿using DU7.CustomTypes;
using DU7.ServerOnly;
using Microsoft.Toolkit.HighPerformance;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace DU7.ViewModels;

public partial class ApplicationViewModel
{
    public async Task Bind()
    {
        try
        {
            if (online)
            {
                throw new InvalidOperationException("Cannot become a server while already online");
            }

            if (Settings.BindAddress is null)
            {
                throw new InvalidOperationException("No bind address provided in the settings");
            }

            mainSocket = new(Settings.BindAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                mainSocket.Bind(new IPEndPoint(Settings.BindAddress, Settings.BindPort));
            }
            catch { throw new InvalidOperationException("Couldn't bind a socket to the provided bind address"); }

            try
            {
                mainSocket.Listen();
            }
            catch
            {
                throw new InvalidOperationException("Failed to start listening for user connections");
            }

            online = server = true;

            await ServerLoop();
        }
        catch(Exception ex)
        {
            LogExceptionToDebug(ex);
        }
        finally
        {
            GoOfflineAndCleanup();
        }
    }

    private (FileInfo, FileStream) MakeNewFile(DirectoryInfo directory, MemoryAmount fileSize)
    {
        string newFileName;
        do
        {
            newFileName = Path.Combine(directory.FullName, Guid.NewGuid().ToString());
        } while (File.Exists(newFileName));

        var stream = new FileStream(newFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan);
        stream.SetLength(checked((long)(ulong)fileSize));

        return (new(newFileName), stream);
    }

    private Regex numberInBracketsAtStringEnd = new(@"\[\d+\]\z", RegexOptions.Compiled);
    private void NamesafeMoveFile(FileInfo fInfo, DirectoryInfo newDirectory, string newName)
    {
        string fullNewName = Path.Combine(newDirectory.FullName, newName);

        while (File.Exists(fullNewName))
        {
            int extensionIndex = fullNewName.LastIndexOf('.');
            if (extensionIndex == -1)
                extensionIndex = fullNewName.Length;

            var match = numberInBracketsAtStringEnd.Match(fullNewName, 0, extensionIndex);
            if (match.Success)
            {
                var fileVersion = ulong.Parse(fullNewName.AsSpan(match.Index + 1, match.Length - 2));

                var beforeFileVersion = fullNewName.AsSpan()[..match.Index];
                var afterFileVersion = fullNewName.AsSpan()[extensionIndex..];

                fullNewName = $"{beforeFileVersion}[{fileVersion + 1}]{afterFileVersion}";
            }
            else
            {
                fullNewName = fullNewName.Insert(extensionIndex, "[1]");
            }
        }

        fInfo.MoveTo(fullNewName, false);
    }

    private Task ServerLoop()
    {
        if (mainSocket is null)
        {
            throw new InvalidOperationException("Socket shouldn't be null");
        }

        ulong myUserID = IDGenerator.MakeID();
        string myUserName = Settings.Username;
        Socket socket = mainSocket;

        var tempFolder = Settings.TempFolder;
        var downloadsFolder = Settings.DownloadsFolder;

        var files = tempFolder.GetFiles();
        foreach (var file in files)
        {
            if (Guid.TryParse(file.Name, out _))
            {
                try { file.Delete(); } catch { }
            }
            else
            {
                AvailableFiles.Add(new(file, IDGenerator.MakeID()));
            }
        }

        Task serverLoop = new(action, TaskCreationOptions.LongRunning);
        serverLoop.Start();
        return serverLoop;

        void action()
        {
            while (true)
            {
                Socket newConnection = socket.Accept();

                Thread handleNewConnectionThread = new(() =>
                {
                    try
                    {
                        HandleNewConnection();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine($"{ex.GetType().Name} occured in server loop:");
                        Debug.WriteLine($" - Message: {ex.Message}");
                        Debug.WriteLine($" - Source: {ex.Source}");
                        Debug.WriteLine($" - Stack trace: {ex.StackTrace}");
                    }
                });
                handleNewConnectionThread.Start();

                void HandleNewConnection()
                {
                    var ns = new NetworkStream(newConnection);
                    var br = new BinaryReader(ns);
                    var bw = new BinaryWriter(ns);

                    var net = (NetworkEnum)br.ReadByte();

                    switch (net)
                    {
                        case NetworkEnum.UserToServer_Connecting:
                            {

                                string newUserName = br.ReadString();
                                if (newUserName.Length is not >= 1 and <= ApplicationConstants.MaximumUsernameLength)
                                    goto default;

                                ulong id = IDGenerator.MakeID();

                                bw.Write((byte)NetworkEnum.PositiveServerResponse);
                                bw.Write(id);

                                var usersCopy = Application.Current.Dispatcher.Invoke(() => Users.Cast<ServerUserViewModel>().ToList());
                                foreach (var user in usersCopy)
                                {
                                    bw.Write((byte)NetworkEnum.ServerToUser_NewUserData);
                                    bw.Write(user.Name);
                                    bw.Write(user.ID);

                                    lock(user)
                                    {
                                        try
                                        {
                                            user.BinaryWriter.Write((byte)NetworkEnum.ServerToUser_NewUserData);
                                            user.BinaryWriter.Write(newUserName);
                                            user.BinaryWriter.Write(id);
                                            user.BinaryWriter.Flush();
                                        } catch { }
                                    }
                                }

                                bw.Write((byte)NetworkEnum.ServerToUser_NewUserData);
                                bw.Write(myUserName);
                                bw.Write(myUserID);

                                foreach (var file in Application.Current.Dispatcher.Invoke(() => AvailableFiles.ToList()))
                                {
                                    bw.Write((byte)NetworkEnum.ServerToUser_SendingFileData);
                                    bw.Write(file.FileName);
                                    bw.Write(file.Size);
                                    bw.Write(file.ID);
                                }

                                ServerUserViewModel newUser = new(newUserName, id, newConnection, ns, bw);
                                new Thread(() =>
                                {
                                    try
                                    {
                                        newUser.Stream.ReadByte();
                                    }
                                    catch { }

                                    var usersCopy = Application.Current.Dispatcher.Invoke(() =>
                                    {
                                        Users.Remove(newUser);
                                        return Users.Cast<ServerUserViewModel>().ToList();
                                    });

                                    foreach (var user in usersCopy)
                                    {
                                        lock (user)
                                        {
                                            try
                                            {
                                                user.BinaryWriter.Write((byte)NetworkEnum.ServerToUser_UserHasDisconnected);
                                                user.BinaryWriter.Write(newUser.ID);
                                                user.BinaryWriter.Flush();
                                            }
                                            catch { }
                                        }
                                    }
                                }).Start();


                                Application.Current?.Dispatcher.Invoke(() =>
                                {
                                    Users.Add(newUser);
                                });

                                bw.Flush();

                                break;
                            }

                        case NetworkEnum.UserToServer_SendingFile:
                            using(newConnection)
                            {
                                ulong userId = br.ReadUInt64();
                                MemoryAmount fileSize = br.ReadUInt64();
                                string fileName = br.ReadString();

                                var user = Application.Current?.Dispatcher.Invoke(() =>
                                {
                                    return Users.SingleOrDefault(u => u.ID == userId);
                                });

                                if (user is not ServerUserViewModel serverUser)
                                {
                                    goto default;
                                }

                                var (fInfo, fStream) = MakeNewFile(tempFolder, fileSize);

                                using (fStream)
                                using (var mem = MyMemoryPool.Rent(64 * 1024))
                                {
                                    Span<byte> buffer = mem.Span;

                                    MemoryAmount amountWritten = 0;
                                    while (amountWritten < fileSize)
                                    {
                                        int read = ns.Read(buffer);
                                        fStream.Write(buffer[..read]);
                                        amountWritten += (ulong)read;
                                    }
                                }

                                bw.Write((byte)NetworkEnum.PositiveServerResponse);
                                bw.Flush();

                                NamesafeMoveFile(fInfo, tempFolder, fileName);
                                ulong fileId = IDGenerator.MakeID();

                                var users = Application.Current.Dispatcher.Invoke(() =>
                                {
                                    AvailableFiles.Add(new(fInfo, fileId));
                                    return Users.Cast<ServerUserViewModel>().ToList();
                                });

                                foreach (var u in users)
                                {
                                    lock (u)
                                    {
                                        u.BinaryWriter.Write((byte)NetworkEnum.ServerToUser_SendingFileData);
                                        u.BinaryWriter.Write(fInfo.Name);
                                        u.BinaryWriter.Write(fileSize);
                                        u.BinaryWriter.Write(fileId);
                                        u.BinaryWriter.Flush();
                                    }
                                }

                                break;
                            }

                        case NetworkEnum.UserToServer_RequestingFile:
                            using(newConnection)
                            {
                                ulong userId = br.ReadUInt64();
                                ulong fileId = br.ReadUInt64();

                                var (user, file) = Application.Current.Dispatcher.Invoke(() =>
                                {
                                    return (
                                        Users.Cast<ServerUserViewModel>().SingleOrDefault(u => u.ID == userId),
                                        AvailableFiles.SingleOrDefault(f => f.ID == fileId));
                                });

                                if (user is null || file is null)
                                {
                                    goto default;
                                }



                                using(var fStream = file.MakeStream())
                                using(var mem = MyMemoryPool.Rent(64 * 1024))
                                {
                                    Span<byte> buffer = mem.Span;
                                    ns.WriteByte((byte)NetworkEnum.PositiveServerResponse);

                                    MemoryAmount fileSize = file.Size;
                                    MemoryAmount sentSize = 0;

                                    while (sentSize < fileSize)
                                    {
                                        int read = fStream.Read(buffer);
                                        ns.Write(buffer[..read]);
                                        sentSize += (ulong)read;
                                    }
                                    ns.Flush();
                                }

                                break;
                            }

                        default:
                            bw.Write((byte)NetworkEnum.NegativeServerResponse);
                            bw.Flush();
                            break;
                    }
                }
            }
        }
    }
}