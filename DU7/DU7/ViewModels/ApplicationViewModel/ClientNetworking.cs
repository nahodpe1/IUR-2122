﻿using DU7.CustomTypes;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace DU7.ViewModels;

public partial class ApplicationViewModel
{
    public async Task Connect()
    {
        try
        {
            await ConnectPrivate();
        }
        catch (Exception ex)
        {
            LogExceptionToDebug(ex);
        }
        finally
        {
            GoOfflineAndCleanup();
        }
    }
    private async Task ConnectPrivate()
    {
        if (online)
        {
            throw new InvalidOperationException("Cannot connect to server while already online");
        }

        if (Settings.ServerAddress is null)
        {
            throw new InvalidOperationException("No server address provided in the settings");
        }

        Trace.Assert(mainSocket is null);



        mainSocket = new(Settings.ServerAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

        string name = Settings.Username;
        try
        {
            using CancellationTokenSource cts = new(5000);
            await mainSocket.ConnectAsync(Settings.ServerAddress, Settings.ServerPort, cts.Token);
        }
        catch { throw new Exception("Couldn't connect to server, possibly wrong IP / port"); }

        mainSocketStream = new NetworkStream(mainSocket);
        mainSocketWriter = new(mainSocketStream);
        mainSocketReader = new(mainSocketStream);



        var getUserIdTask = Task.Run(() =>
        {
            mainSocketWriter.Write((byte)NetworkEnum.UserToServer_Connecting);
            mainSocketWriter.Write(name);
            mainSocketWriter.Flush();
            if ((NetworkEnum)mainSocketReader.ReadByte() != NetworkEnum.PositiveServerResponse)
            {
                throw new Exception("Server did not accept the connection");
            }
            return mainSocketReader.ReadUInt64();
        });


        myUserID = await getUserIdTask;
        (online, server) = (true, false);

        await ClientLoop();
    }

    private Task ClientLoop()
    {
        var task = new Task(clientLoop, TaskCreationOptions.LongRunning);
        task.Start();
        return task;

        void clientLoop()
        {
            if (mainSocketReader is null)
            {
                throw new InvalidOperationException("Socket shouldn't be null");
            }

            while (true)
            {
                switch ((NetworkEnum)mainSocketReader.ReadByte())
                {
                    case NetworkEnum.ServerToUser_NewUserData:
                        {
                            string name = mainSocketReader.ReadString();
                            ulong id = mainSocketReader.ReadUInt64();

                            Application.Current?.Dispatcher.Invoke(() =>
                            {
                                if (Users.All(u => u.ID != id))
                                {
                                    Users.Add(new(name, id));
                                }
                            });

                            break;
                        }

                    case NetworkEnum.ServerToUser_UserHasDisconnected:
                        {
                            ulong id = mainSocketReader.ReadUInt64();

                            Application.Current?.Dispatcher.Invoke(() =>
                            {
                                var user = Users.Single(u => u.ID == id);
                                Users.Remove(user);
                            });

                            break;
                        }

                    case NetworkEnum.ServerToUser_SendingFileData:
                        {
                            string name = mainSocketReader.ReadString();
                            MemoryAmount size = mainSocketReader.ReadUInt64();
                            ulong fileID = mainSocketReader.ReadUInt64();

                            Application.Current?.Dispatcher.Invoke(() =>
                            {
                                if (AvailableFiles.All(f => f.ID != fileID))
                                {
                                    AvailableFiles.Add(new(name, size, fileID));
                                }
                            });
                        }
                        break;

                    case NetworkEnum.ServerToUser_FileWasDeleted:
                        {
                            ulong id = mainSocketReader.ReadUInt64();

                            Application.Current?.Dispatcher.Invoke(() =>
                            {
                                var file = AvailableFiles.Single(f => f.ID == id);
                                AvailableFiles.Remove(file);
                            });
                        }
                        break;
                }

            }
        }

    }
}
