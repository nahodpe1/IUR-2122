﻿using System.Collections.ObjectModel;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace DU7.ViewModels;

public partial class ApplicationViewModel : ViewModelBase
{
    private volatile bool online = false;
    private volatile bool connecting = false;
    private volatile bool server = false;

    private ulong myUserID;

    private Socket? mainSocket;
    private Stream? mainSocketStream;
    private BinaryReader? mainSocketReader;
    private BinaryWriter? mainSocketWriter;

    private void GoOfflineAndCleanup()
    {

        try { mainSocket?.Disconnect(false); } catch { }
        mainSocket?.Dispose();
        mainSocketStream?.Dispose();
        mainSocketReader?.Dispose();
        mainSocketWriter?.Dispose();

        myUserID = default;
        connecting = online = server = false;

        mainSocket = null;
        mainSocketStream = null;
        mainSocketReader = null;
        mainSocketWriter = null;

        Users.Clear();
        Tasks.Clear();
        AvailableFiles.Clear();
    }

    public ApplicationViewModel()
    {
        Settings = new(() => !connecting && !online);
        InitializeDownloadCommand();
        InitializeUploadCommand();
        InitializeConnectCommand();
        InitializeHostCommand();
        InitializeGoOfflineCommand();

        if (Settings.AutoHost && HostCommand.CanExecute(null))
        {
            HostCommand.Execute(null);
        }
        else if (Settings.AutoConnect && ConnectCommand.CanExecute(null))
        {
            ConnectCommand.Execute(null);
        }
    }
    public SettingsViewModel Settings { get; }
    public ObservableCollection<ConnectedUserViewModel> Users { get; } = new()
    {
        //new("Test 1", 0),
        //new("test 2", 2),
        //new("PETRUSION", 4)
    };
    public ObservableCollection<NetworkTaskProgressViewModel> Tasks { get; } = new()
    {
        //new(1651654684, "test1", 50, null, NetworkDirection.Download),
        //new(254531432, "test2kjdnsfkjfsgnjkfdsn.png", 54, null, NetworkDirection.Upload)
    };
    public ObservableCollection<AvailableFileViewModel> AvailableFiles { get; } = new()
    {
        //new("test", 156134354, 50),
        //new("test2", 156134354, 50),
        //new("testlooooooooooooooooong", 156134354, 50),
        //new("testkfjnkdsjgnkdsjfnkdsfjndskjfndskjfndskjfn", 156134354, 50),
    };
}
