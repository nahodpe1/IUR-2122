﻿using System;

namespace DU7.ViewModels;

public partial class ApplicationViewModel : IDisposable
{
    public void Dispose()
    {
        Settings.Dispose();
        mainSocket?.Dispose();

        foreach (var user in Users)
        {
            if (user is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }
    }
}
