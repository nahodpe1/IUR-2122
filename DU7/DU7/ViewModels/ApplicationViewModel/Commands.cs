﻿using DU7.CustomTypes;
using DU7.ServerOnly;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using static System.Formats.Asn1.AsnWriter;
using Button = System.Windows.Controls.Button;

namespace DU7.ViewModels;

public partial class ApplicationViewModel : ViewModelBase
{
    [MemberNotNull(nameof(DownloadFileCommand))]
    private void InitializeDownloadCommand()
    {
        DownloadFileCommand = new RelayCommand(
            execute:
            (o) =>
            {
                if (o is not AvailableFileViewModel file)
                {
                    throw new ArgumentException($"Expected an {nameof(AvailableFileViewModel)} object");
                }

                if (!online)
                {
                    throw new InvalidOperationException("Should be online when downloading a file");
                }

                if (server)
                {
                    DownloadFileAsServer(file);
                }
                else
                {
                    DownloadFileAsClient(file);
                }
            },
            canExecute:
            (o) =>
            {
                return online &&
                o is AvailableFileViewModel file &&
                AvailableFiles.Contains(file) &&
                Tasks.All(t => t.FileID != file.ID);
            });
    }
    [MemberNotNull(nameof(UploadFileCommand))]
    private void InitializeUploadCommand()
    {
        UploadFileCommand = new RelayCommand(
            execute:
            _ =>
            {
                var dialog = new OpenFileDialog()
                {
                    CheckFileExists = true,
                    Multiselect = false
                };

                if (dialog.ShowDialog() == DialogResult.OK && UploadFileCommand!.CanExecute(null))
                {
                    FileInfo fInfo = new(dialog.FileName);

                    if (server)
                    {
                        UploadFileAsServer(fInfo);
                    }
                    else
                    {
                        UploadFileAsClient(fInfo);
                    }
                }
            },
            canExecute:
            _ =>
            {
                return online;
            });
    }

    [MemberNotNull(nameof(ConnectCommand))]
    private void InitializeConnectCommand()
    {
        ConnectCommand = new RelayCommand(
            execute:
            async _ =>
            {
                connecting = true;
                await Connect();
                ((RelayCommand)HostCommand!).OnCanExecuteChanged();
                ((RelayCommand)ConnectCommand!).OnCanExecuteChanged();
                ((RelayCommand)GoOfflineCommand!).OnCanExecuteChanged();
            },
            canExecute:
            _ =>
            {
                return !connecting && !online && Settings.ServerAddress is not null;
            });
    }

    [MemberNotNull(nameof(HostCommand))]
    private void InitializeHostCommand()
    {
        HostCommand = new RelayCommand(
            execute: async _ =>
            {
                connecting = true;
                await Bind();
                ((RelayCommand)HostCommand!).OnCanExecuteChanged();
                ((RelayCommand)ConnectCommand!).OnCanExecuteChanged();
                ((RelayCommand)GoOfflineCommand!).OnCanExecuteChanged();
            },
            canExecute:
            _ =>
            {
                return !connecting && !online && Settings.BindAddress is not null;
            });
    }

    [MemberNotNull(nameof(GoOfflineCommand))]
    private void InitializeGoOfflineCommand()
    {
        GoOfflineCommand = new RelayCommand(
        _ =>
        {
            GoOfflineAndCleanup();
            ((RelayCommand)HostCommand!).OnCanExecuteChanged();
            ((RelayCommand)ConnectCommand!).OnCanExecuteChanged();
            ((RelayCommand)GoOfflineCommand!).OnCanExecuteChanged();
        },
        _ =>
        {
            return online && Tasks.Count == 0;
        });
    }

    public ICommand DownloadFileCommand { get; private set; }
    public ICommand UploadFileCommand { get; private set; }
    public ICommand ConnectCommand { get; private set; }
    public ICommand HostCommand { get; private set; }
    public ICommand GoOfflineCommand { get; private set; }


    [Conditional("DEBUG")]
    private static void LogExceptionToDebug(Exception ex, [CallerMemberName] string name = null!)
    {
        Debug.WriteLine($"{ex.GetType().Name} occured in {name}: {ex.Message}");
    }

    private const int keepNetTaskAfterFinishingMilliseconds = 5000;
    private const int uiUpdatePeriodMilliseconds = 10;

    private static Task TransferData(Stream inputStream, Stream outputStream, NetworkTaskProgressViewModel netTask)
    {
        TaskCreationOptions options =
            netTask.TotalSize > 500 * MemoryAmount.MB
            ? TaskCreationOptions.LongRunning
            : TaskCreationOptions.None;

        var task = new Task(action, netTask.CancelToken, options);
        task.Start();
        return task;

        void action()
        {
            var token = netTask.CancelToken;

            token.ThrowIfCancellationRequested();

            using var mem = MyMemoryPool.Rent(64 * 1024);

            var buffer = mem.Span;

            var stopw = Stopwatch.StartNew();
            MemoryAmount doneAmount = 0;
            while (doneAmount < netTask.TotalSize)
            {
                token.ThrowIfCancellationRequested();

                int read = inputStream.Read(buffer);
                outputStream.Write(buffer[..read]);
                doneAmount += (ulong)read;
                if (stopw.ElapsedMilliseconds > uiUpdatePeriodMilliseconds)
                {
                    stopw.Restart();
                    netTask.DoneSize = doneAmount;
                }
            }
            stopw.Stop();
            netTask.DoneSize = doneAmount;
        }
    }

    private async void DownloadFileAsClient(AvailableFileViewModel file)
    {
        NetworkTaskProgressViewModel netTask = new(file.Size, file.FileName, file.ID, null, NetworkDirection.Download);

        Tasks.Add(netTask);

        try
        {
            await Task.Run(async () =>
            {
                using Socket s = new(SocketType.Stream, ProtocolType.Tcp);
                s.Connect(new IPEndPoint(Settings.ServerAddress!, Settings.ServerPort));

                NetworkStream ns = new(s);
                BinaryReader br = new(ns);
                BinaryWriter bw = new(ns);

                bw.Write((byte)NetworkEnum.UserToServer_RequestingFile);
                bw.Write(myUserID);
                bw.Write(netTask.FileID);
                bw.Flush();

                if ((NetworkEnum)br.ReadByte() != NetworkEnum.PositiveServerResponse)
                    return;

                var downloads = Settings.DownloadsFolder;

                var (newFileInfo, newFileStream) = MakeNewFile(downloads, netTask.TotalSize);

                try
                {
                    using (newFileStream)
                    using (var mem = MyMemoryPool.Rent(64 * 1024))
                        await TransferData(ns, newFileStream, netTask);
                }
                catch
                {
                    try { newFileInfo.Delete(); } catch { }

                    return;
                }

                NamesafeMoveFile(newFileInfo, downloads, netTask.FileName);

                netTask.MarkFinished();
            });

            await Task.Delay(keepNetTaskAfterFinishingMilliseconds);
        }
        catch (Exception ex)
        {
            LogExceptionToDebug(ex);
        }
        finally
        {
            Tasks.Remove(netTask);
        }
    }
    private async void DownloadFileAsServer(AvailableFileViewModel file)
    {
        NetworkTaskProgressViewModel netTask = new(file.Size, file.FileName, file.ID, null, NetworkDirection.Download);

        Tasks.Add(netTask);

        try
        {
            var downloads = Settings.DownloadsFolder;

            await Task.Run(async () =>
            {
                var (newFileInfo, newFileStream) = MakeNewFile(downloads, netTask.TotalSize);

                try
                {
                    using (newFileStream)
                    using (var originalFileStream = file.MakeStream())
                    using (var mem = MyMemoryPool.Rent(64 * 1024))
                        await TransferData(originalFileStream, newFileStream, netTask);
                }
                catch
                {
                    try { newFileInfo.Delete(); } catch { }
                    return;
                }

                NamesafeMoveFile(newFileInfo, downloads, netTask.FileName);

                netTask.MarkFinished();
            });

            await Task.Delay(keepNetTaskAfterFinishingMilliseconds);
        }
        catch (Exception ex)
        {
            LogExceptionToDebug(ex);
        }
        finally
        {
            Tasks.Remove(netTask);
        }
    }
    private async void UploadFileAsClient(FileInfo file)
    {
        NetworkTaskProgressViewModel netTask = new((ulong)file.Length, file.Name, default, null, NetworkDirection.Upload);

        Tasks.Add(netTask);

        try
        {
            await Task.Run(async () =>
            {
                using Socket s = new(SocketType.Stream, ProtocolType.Tcp);
                s.Connect(new IPEndPoint(Settings.ServerAddress!, Settings.ServerPort));

                NetworkStream ns = new(s);
                BinaryReader br = new(ns);
                BinaryWriter bw = new(ns);

                bw.Write((byte)NetworkEnum.UserToServer_SendingFile);
                bw.Write(myUserID);
                bw.Write(netTask.TotalSize);
                bw.Write(netTask.FileName);
                bw.Flush();

                try
                {
                    using (var fStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.SequentialScan))
                        await TransferData(fStream, ns, netTask);
                    ns.Flush();
                }
                catch
                {
                    return;
                }

                Task<NetworkEnum> waitForNetworkEnum = Task.Run(() => (NetworkEnum)br.ReadByte());

                if (await waitForNetworkEnum.WaitAsync(TimeSpan.FromSeconds(15)) == NetworkEnum.PositiveServerResponse)
                {
                    netTask.MarkFinished();
                }
            });

            await Task.Delay(keepNetTaskAfterFinishingMilliseconds);
        }
        catch (Exception ex)
        {
            LogExceptionToDebug(ex);
        }
        finally
        {
            Tasks.Remove(netTask);
        }
    }
    private async void UploadFileAsServer(FileInfo file)
    {
        NetworkTaskProgressViewModel netTask = new((ulong)file.Length, file.Name, IDGenerator.MakeID(), null, NetworkDirection.Upload);

        Tasks.Add(netTask);

        try
        {
            var temps = Settings.TempFolder;

            var newFileViewModel = await Task.Run(async () =>
            {
                var (newFileInfo, newFileStream) = MakeNewFile(temps, netTask.TotalSize);

                try
                {
                    using (newFileStream)
                    using (var originalFileStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.SequentialScan))
                        await TransferData(originalFileStream, newFileStream, netTask);
                }
                catch
                {
                    try { newFileInfo.Delete(); } catch { }
                    return null;
                }

                NamesafeMoveFile(newFileInfo, temps, netTask.FileName);

                netTask.MarkFinished();

                var viewModel = new AvailableFileViewModel(newFileInfo, netTask.FileID);

                foreach (var user in System.Windows.Application.Current.Dispatcher.Invoke(() => Users.Cast<ServerUserViewModel>().ToList()))
                {
                    lock (user)
                    {
                        try
                        {
                            user.BinaryWriter.Write((byte)NetworkEnum.ServerToUser_SendingFileData);
                            user.BinaryWriter.Write(viewModel.FileName);
                            user.BinaryWriter.Write(viewModel.Size);
                            user.BinaryWriter.Write(viewModel.ID);
                            user.BinaryWriter.Flush();
                        }
                        catch { }
                    }
                }

                return viewModel;
            });

            if(newFileViewModel is not null)
            {
                AvailableFiles.Add(newFileViewModel);
            }

            await Task.Delay(keepNetTaskAfterFinishingMilliseconds);
        }
        catch (Exception ex)
        {
            LogExceptionToDebug(ex);
        }
        finally
        {
            Tasks.Remove(netTask);
        }
    }
}
