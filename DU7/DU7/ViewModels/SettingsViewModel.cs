﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml.Linq;

namespace DU7.ViewModels
{
    public class SettingsViewModel : ViewModelBase, IDisposable
    {
        private readonly DirectoryInfo programData;
        private DirectoryInfo tempFolder;
        private DirectoryInfo downloadsFolder;
        private readonly FileInfo programDataFile;
        private bool autoConnect, autoHost;
        private string username;
        private IPAddress? serverAddress, bindAddress;
        private int serverPort, bindPort;
        private MemoryAmount maximumTempFolderSize;

        private Func<bool> canEditSettings;
        private void CheckCanEditSettigns()
        {
            if (!canEditSettings())
            {
                throw new InvalidOperationException("Cannot change settings while online");
            }
        }

        public SettingsViewModel(Func<bool> canEditSettings)
        {
            this.canEditSettings = canEditSettings;

            programData = new(
                Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), 
                    "File Sender Data"));

            programDataFile = new FileInfo(Path.Combine(programData.FullName, "programData.xml"));

            if (!programData.Exists)
            {
                programData.Create();
            }
            else if (programDataFile.Exists)
            {
                var doc = XDocument.Parse(File.ReadAllText(programDataFile.FullName));

                if (doc is { Root: { Name.LocalName: "Settings" } root })
                {
                    autoConnect = root.Element(nameof(AutoConnect))?.Value is "1";
                    autoHost = root.Element(nameof(AutoHost))?.Value is "1";
                    if (autoConnect && autoHost)
                    {
                        AutoConnect = AutoHost = false;
                    }
                    else
                    {
                        AutoConnect = autoConnect;
                        AutoHost = autoHost;
                    }

                    try { TempFolderPath = root.Element(nameof(TempFolderPath))?.Value ?? ""; } catch { }
                    try { DownloadsFolderPath = root.Element(nameof(DownloadsFolderPath))?.Value ?? ""; } catch { }
                    try { Username = root.Element(nameof(Username))?.Value!; } catch { }
                    try { ServerAddressString = root.Element(nameof(ServerAddressString))?.Value ?? ""; } catch { }
                    try { BindAddressString = root.Element(nameof(BindAddressString))?.Value ?? ""; } catch { }
                    if (int.TryParse(root.Element(nameof(ServerPort))?.Value, out int parsedServerPort))
                    {
                        ServerPort = parsedServerPort;
                    }
                    if (int.TryParse(root.Element(nameof(BindPort))?.Value, out int parsedBindPort))
                    {
                        BindPort = parsedBindPort;
                    }
                    if (ulong.TryParse(root.Element(nameof(MaximumTempFolderSizeGB))?.Value, out ulong gb))
                    {
                        MaximumTempFolderSizeGB = gb;
                    }
                }
            }


            tempFolder ??= new(
                Path.Combine(
                    programData.FullName,
                    "Temporary Files"));

            if (!tempFolder.Exists)
            {
                tempFolder.Create();
            }

            downloadsFolder ??= new(
                GetDownloadsPath()
                ??
                Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    "File Sender Downloaded Files"
                    ));

            if (!downloadsFolder.Exists)
            {
                downloadsFolder.Create();
            }

            username ??= "Unnamed";

            if (MaximumTempFolderSizeGB == 0)
                MaximumTempFolderSizeGB = 1;

            InitializeChangeDownloadsFolderCommand();
            InitializeChangeTempFolderCommand();
        }

        public void SaveSettigns()
        {
            var doc = new XDocument(
                new XElement("Settings",
                    new []
                    {
                        new XElement(nameof(AutoConnect), AutoConnect ? "1" : "0"),
                        new XElement(nameof(AutoHost), AutoHost ? "1" : "0"),
                        new XElement(nameof(TempFolderPath), TempFolderPath),
                        new XElement(nameof(DownloadsFolderPath), DownloadsFolderPath),
                        new XElement(nameof(Username), Username),
                        new XElement(nameof(ServerAddressString), ServerAddressString),
                        new XElement(nameof(BindAddressString), BindAddressString),
                        new XElement(nameof(ServerPort), ServerPort),
                        new XElement(nameof(BindPort), BindPort),
                        new XElement(nameof(MaximumTempFolderSizeGB), MaximumTempFolderSizeGB)
                    }));

            doc.Save(programDataFile.FullName);
        }

        public ICommand ChangeDownloadsFolderCommand { get; private set; }
        public ICommand ChangeTempFolderCommand { get; private set; }

        [MemberNotNull(nameof(ChangeDownloadsFolderCommand))]
        private void InitializeChangeDownloadsFolderCommand()
        {
            ChangeDownloadsFolderCommand = new RelayCommand(
                execute:
                (window) =>
                {
                    var dialog = new FolderBrowserDialog()
                    {
                        ShowNewFolderButton = true,
                        InitialDirectory = DownloadsFolderPath
                    };

                    if (dialog.ShowDialog() == DialogResult.OK && ChangeDownloadsFolderCommand!.CanExecute(null))
                    {
                        DownloadsFolderPath = dialog.SelectedPath;
                    }
                },
                canExecute:
                _ =>
                {
                    return canEditSettings();
                });
        }

        [MemberNotNull(nameof(ChangeTempFolderCommand))]
        private void InitializeChangeTempFolderCommand()
        {
            ChangeTempFolderCommand = new RelayCommand(
                execute:
                (window) =>
                {
                    var dialog = new FolderBrowserDialog()
                    {
                        ShowNewFolderButton = true,
                        InitialDirectory = TempFolderPath
                    };

                    if (dialog.ShowDialog() == DialogResult.OK && ChangeTempFolderCommand!.CanExecute(null))
                    {
                        TempFolderPath = dialog.SelectedPath;
                    }
                },
                canExecute:
                _ =>
                {
                    return canEditSettings();
                });
        }

        public int MaxUsernameLength => ApplicationConstants.MaximumUsernameLength;

        public DirectoryInfo TempFolder { get => tempFolder; set => tempFolder = value ?? throw new ArgumentNullException(); }
        public string TempFolderPath 
        { 
            get => tempFolder.FullName;
            set
            {
                CheckCanEditSettigns();

                if (value is null)
                    throw new ArgumentNullException(nameof(value));

                if (!Directory.Exists(value))
                {
                    throw new ArgumentException("Directory doesn't exist");
                }

                DirectoryInfo dir = new(value);

                if (dir.FullName != downloadsFolder?.FullName)
                {
                    tempFolder = dir;
                    InvokePropertyChanged();
                }
            }
        }
        public DirectoryInfo DownloadsFolder { get => downloadsFolder; set => downloadsFolder = value ?? throw new ArgumentNullException(); }
        public string DownloadsFolderPath 
        { 
            get => downloadsFolder.FullName;
            set
            {
                CheckCanEditSettigns();
                if (value is null)
                    throw new ArgumentNullException(nameof(value));

                if (!Directory.Exists(value))
                {
                    throw new ArgumentException("Directory doesn't exist");
                }

                DirectoryInfo dir = new(value);

                if (dir.FullName != (downloadsFolder?.FullName ?? ""))
                {
                    downloadsFolder = dir;
                    InvokePropertyChanged();
                }
            }
        }
        public bool AutoConnect 
        { 
            get => autoConnect;
            set
            {
                UpdatePropertyIfNotEqual(ref autoConnect, value);
                if (autoConnect & autoHost)
                {
                    AutoHost = false;
                }
            }
        }
        public bool AutoHost 
        {
            get => autoHost;
            set
            {
                UpdatePropertyIfNotEqual(ref autoHost, value);
                if (autoConnect & autoHost)
                {
                    AutoConnect = false;
                }
            }
        }

        private static readonly Regex regexNotAlphanumericNorSpace = new(@"[^0-9a-zA-Z ]", RegexOptions.Compiled);
        private static readonly Regex regexAlphanumeric = new(@"[a-zA-Z]");
        public string Username 
        { 
            get => username;
            set
            {
                CheckCanEditSettigns();
                if (username == value)
                    return;

                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("You must enter a username");
                }

                if (value.Length > ApplicationConstants.MaximumUsernameLength)
                {
                    throw new ArgumentException($"Your username must not be longer than {ApplicationConstants.MaximumUsernameLength} characters");
                }

                if (regexNotAlphanumericNorSpace.IsMatch(value))
                {
                    throw new ArgumentException("Your username must only consist of alphanumeric characters and spaces");
                }

                if (value[0] == ' ' || value[^1] == ' ')
                {
                    throw new ArgumentException("Your username must not start or end with a space");
                }

                if (value.Contains("  "))
                {
                    throw new ArgumentException("Your username must not contain more than one space between alphanumeric characters");
                }

                if (!regexAlphanumeric.IsMatch(value))
                {
                    throw new ArgumentException("Your username must contain at least one alphanumeric character");
                }

                username = value;
                InvokePropertyChanged();
            }
        }
        public IPAddress? ServerAddress 
        {
            get => serverAddress; set
            {
                CheckCanEditSettigns();
                serverAddress = value;
            } 
        }
        public string ServerAddressString 
        { 
            get => serverAddress?.ToString() ?? ""; 
            set
            {
                CheckCanEditSettigns();
                if (IPAddress.TryParse(value, out var address))
                {
                    UpdatePropertyIfNotEqual(ref serverAddress, address);
                }
                else
                {
                    throw new ArgumentException("Couldn't parse IP Address");
                }
            }
        }
        public IPAddress? BindAddress 
        {
            get => bindAddress;
            set
            {
                CheckCanEditSettigns();
                bindAddress = value;
            }
        }
        public string BindAddressString
        { 
            get => bindAddress?.ToString() ?? "";
            set
            {
                CheckCanEditSettigns();
                if (IPAddress.TryParse(value, out var address))
                {
                    UpdatePropertyIfNotEqual(ref bindAddress, address);
                }
                else
                {
                    throw new ArgumentException("Couldn't parse IP Address");
                }
            }
        }
        public int ServerPort 
        { 
            get => serverPort;
            set
            {
                CheckCanEditSettigns();
                if (value is >= 0 and <= 65535)
                {
                    UpdatePropertyIfNotEqual(ref serverPort, value);
                }
                else throw new ArgumentException("Port must be in 0-65365 range");
            }
        }
        public int BindPort
        { 
            get => bindPort;
            set
            {
                CheckCanEditSettigns();
                if (value is >= 0 and <= 65535)
                {
                    UpdatePropertyIfNotEqual(ref bindPort, value);
                }
                else throw new ArgumentException("Port must be in 0-65365 range");
            }
        }
        public ulong MaximumTempFolderSizeGB
        {
            get => maximumTempFolderSize.GigaBytes;
            set
            {
                CheckCanEditSettigns();
                try
                {
                    const ulong bytesInGigabyte = 1024 * 1024 * 1024;
                    UpdatePropertyIfNotEqual(ref maximumTempFolderSize, checked(bytesInGigabyte * value));
                }
                catch (OverflowException e)
                {
                    throw new ArgumentException($"Size too large, maximum is {ulong.MaxValue >> 30}GB", e);
                }
            }
        }


        private static Guid DownloadsFolderGuid = new("374DE290-123F-4565-9164-39C4925E467B");
        private bool disposedValue;

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        private static extern int SHGetKnownFolderPath(ref Guid id, int flags, IntPtr token, out IntPtr path);
        public static string? GetDownloadsPath()
        {
            if (Environment.OSVersion.Version.Major < 6)
                return null;

            IntPtr pathPtr = IntPtr.Zero;

            try
            {
                SHGetKnownFolderPath(ref DownloadsFolderGuid, 0, IntPtr.Zero, out pathPtr);
                return Marshal.PtrToStringUni(pathPtr);
            }
            finally
            {
                Marshal.FreeCoTaskMem(pathPtr);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    
                }

                SaveSettigns();

                disposedValue = true;
            }
        }

        ~SettingsViewModel()
        {
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
