﻿using System;
using System.Windows.Input;

namespace DU7.ViewModels;

public class RelayCommand : ICommand
{
    private readonly Action<object?> execute;
    private readonly Predicate<object?> canExecute;

    private event EventHandler? CanExecuteChangedPrivate;

    public RelayCommand(Action<object?> execute) : this(execute, DefaultCanExecute)
    {
    }

    public RelayCommand(Action<object?> execute, Predicate<object?> canExecute)
    {
        this.execute = execute ?? throw new ArgumentNullException(nameof(execute));
        this.canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
    }

    public event EventHandler? CanExecuteChanged
    {
        add
        {
            CommandManager.RequerySuggested += value;
            CanExecuteChangedPrivate += value;
        }

        remove
        {
            CommandManager.RequerySuggested -= value;
            CanExecuteChangedPrivate -= value;
        }
    }

    public bool CanExecute(object? parameter)
    {
        return canExecute is not null && canExecute(parameter);
    }

    public void Execute(object? parameter)
    {
        execute(parameter);
    }

    public void OnCanExecuteChanged()
    {
        CanExecuteChangedPrivate?.Invoke(this, EventArgs.Empty);
    }

    //zjednoduseni, kdyz mam vzdy povoleno provadet prikaz
    private static bool DefaultCanExecute(object? parameter)
    {
        return true;
    }
}
