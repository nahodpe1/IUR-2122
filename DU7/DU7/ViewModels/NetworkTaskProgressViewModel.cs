﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;

namespace DU7.ViewModels;

public class NetworkTaskProgressViewModel : ViewModelBase
{
    private MemoryAmount doneSize;
    private volatile bool finished;

    public NetworkTaskProgressViewModel(MemoryAmount totalSize, string fileName, ulong fileId, Image? specialImage, NetworkDirection direction)
    {
        doneSize = 0;
        finished = false;
        TotalSize = totalSize;
        FileName = fileName ?? throw new ArgumentNullException(nameof(fileName));
        FileID = fileId;
        SpecialImage = specialImage;
        NetworkDirection = direction;
        cancelTokenSource = new();

        CancelCommand = new RelayCommand(
            _ =>
            {
                cancelTokenSource.Cancel();
            },
            _ =>
            {
                return !cancelTokenSource.IsCancellationRequested && !finished && doneSize < TotalSize;
            });
    }

    public ICommand CancelCommand { get; }

    private CancellationTokenSource cancelTokenSource;
    public CancellationToken CancelToken => cancelTokenSource.Token;
    public void MarkFinished()
    {
        bool previousValue = finished;
        finished = true;
        if (!previousValue && DoneSize == TotalSize)
        {
            InvokePropertyChanged(nameof(PercentageDone));
        }
    }
    public bool Finished => finished;
    public ulong FileID { get; }
    public unsafe MemoryAmount DoneSize 
    {
        get 
        {
            Debug.Assert(sizeof(MemoryAmount) == sizeof(ulong));
            return Interlocked.Read(ref Unsafe.As<MemoryAmount, ulong>(ref doneSize));
        } 
        set
        {
            Debug.Assert(sizeof(MemoryAmount) == sizeof(ulong));
            ulong previous = Interlocked.Exchange(ref Unsafe.As<MemoryAmount, ulong>(ref doneSize), value);

            if (previous != value)
            {
                InvokePropertyChanged();
                InvokePropertyChanged(nameof(PercentageDone));
            }
        }
    }
    public MemoryAmount TotalSize { get; }
    public string FileName { get; }
    public Image? SpecialImage { get; }
    public double PercentageDone => Math.Min((double)DoneSize / TotalSize * 100d, finished ? 100d : 99.99d);
    public NetworkDirection NetworkDirection { get; }
}
