﻿using System;
using System.IO;
using System.Net.Sockets;

namespace DU7.ViewModels;

public record ConnectedUserViewModel(string Name, ulong ID);

public record ServerUserViewModel : ConnectedUserViewModel, IDisposable
{
    public ServerUserViewModel(string Name, ulong ID, Socket socket) : base(Name, ID)
    {
        Socket = socket;
        Stream = new NetworkStream(socket);
        BinaryWriter = new BinaryWriter(Stream);
    }
    public ServerUserViewModel(string Name, ulong ID, Socket socket, Stream stream, BinaryWriter writer) : base(Name, ID)
    {
        Socket = socket;
        Stream = stream;
        BinaryWriter = writer;
    }

    public Socket Socket { get; }
    public Stream Stream { get; }
    public BinaryWriter BinaryWriter { get; }

    public void Dispose()
    {
        Socket.Dispose();
    }
}
