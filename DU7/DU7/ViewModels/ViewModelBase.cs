﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace DU7.ViewModels;

public abstract class ViewModelBase : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler? PropertyChanged;

    private static readonly Dictionary<string, PropertyChangedEventArgs> argsDictionary = new();

    
    //private void InvokePropertyChangedInvokeless(string name)
    //{
    //    if (!argsDictionary.TryGetValue(name, out var args))
    //    {
    //        argsDictionary[name] = args = new(name);
    //    }
    //    PropertyChanged?.Invoke(this, args);
    //}
    protected void InvokePropertyChanged([CallerMemberName] string name = null!)
    {
        //if (Application.Current.Dispatcher.CheckAccess())
        //{
        //    InvokePropertyChangedInvokeless(name);
        //}
        //else
        //{
        //    Application.Current?.Dispatcher.InvokeAsync(() => InvokePropertyChangedInvokeless(name), System.Windows.Threading.DispatcherPriority.Background);
        //}

        if (!argsDictionary.TryGetValue(name, out var args))
        {
            argsDictionary[name] = args = new(name);
        }
        PropertyChanged?.Invoke(this, args);
    }

    protected void UpdatePropertyIfNotEqual<T>(ref T field, T newValue, [CallerMemberName] string name = null!)
    {
#pragma warning disable IDE0038 // Use pattern matching
        // using pattern matching here would result in value types being unnecessarily boxed

        bool equal =
            field is IEquatable<T>
            ? ((IEquatable<T>)field).Equals(newValue)
            : EqualityComparer<T>.Default.Equals(field, newValue);

#pragma warning restore IDE0038 // Use pattern matching

        if (!equal)
        {
            field = newValue;
            InvokePropertyChanged(name);
        }
    }
}
