﻿using System;
using System.IO;
using System.Windows.Controls;

namespace DU7.ViewModels;

public class AvailableFileViewModel : IDisposable
{
    public AvailableFileViewModel(FileInfo info, ulong id)
    {
        ID = id;
        this.info = info;
        if (!info.Exists)
        {
            throw new ArgumentException("File does not exist");
        }
        FileName = info.Name;
        Size = (ulong)info.Length;
        

        int index = FileName.LastIndexOf('.');
        if (index != -1)
        {
            SpecialImage = FileName[++index..] switch
            {
                _ => null
            };
        }

        stream = MakeStream();
    }

    public AvailableFileViewModel(string filename, MemoryAmount size, ulong id)
    {
        ID = id;
        FileName = filename;
        Size = size;

        int index = FileName.LastIndexOf('.');
        if (index != -1)
        {
            SpecialImage = FileName[++index..] switch
            {
                _ => null
            };
        }
    }

    // stream dummy to keep file open
    private readonly FileStream? stream;
    private readonly FileInfo? info;

    public string FileName { get; }
    public ulong ID { get; }
    private Image? SpecialImage { get; }
    public MemoryAmount Size { get; }
    public FileStream MakeStream() 
        => new(info?.FullName ?? throw new InvalidOperationException("Attempting to make filestream without fileinfo"), 
            FileMode.Open, FileAccess.Read, FileShare.Read, 64 * 1024, 
            FileOptions.SequentialScan | FileOptions.Asynchronous);
    public FileInfo? FileInfo => info;

    public void Delete()
    {
        Dispose();
        if(info is null)
        {
            throw new InvalidOperationException("Attempting to remove file without fileinfo");
        }
        info.Delete();
    }

    public void Dispose()
    {
        stream?.Dispose();
    }
}