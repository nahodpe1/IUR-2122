﻿using System.Windows;
using System.Windows.Controls;

namespace DU7
{
    public class ConnectedUserCustomControl : Button
    {
        static ConnectedUserCustomControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ConnectedUserCustomControl), new FrameworkPropertyMetadata(typeof(ConnectedUserCustomControl)));
        }

        public static readonly DependencyProperty
            UserNameProperty = DependencyProperty.Register(nameof(UserName), typeof(string), typeof(ConnectedUserCustomControl));

        public event RoutedEventHandler?
            UserNameChanged;

        public static readonly RoutedEvent
            UserNameChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(UserNameChanged), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ConnectedUserCustomControl));

        public string UserName
        {
            get => (string)GetValue(UserNameProperty);
            set => SetValue(UserNameProperty, value);
        }
    }
}
