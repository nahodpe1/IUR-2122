﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DU7
{
    public enum NetworkDirection
    {
        Upload,
        Download
    }
    public class NetworkTaskCustomControl : Button
    {
        static NetworkTaskCustomControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NetworkTaskCustomControl), new FrameworkPropertyMetadata(typeof(NetworkTaskCustomControl)));
            
        }

        public static readonly DependencyProperty
            NetworkDirectionProperty = DependencyProperty.Register(nameof(NetworkDirection), typeof(NetworkDirection), typeof(NetworkTaskCustomControl)),
            DoneSizeProperty = DependencyProperty.Register(nameof(DoneSize), typeof(MemoryAmount), typeof(NetworkTaskCustomControl)),
            TotalSizeProperty = DependencyProperty.Register(nameof(TotalSize), typeof(MemoryAmount), typeof(NetworkTaskCustomControl)),
            PercentageProperty = DependencyProperty.Register(nameof(Percentage), typeof(double), typeof(NetworkTaskCustomControl)),
            FileNameProperty = DependencyProperty.Register(nameof(FileName), typeof(string), typeof(NetworkTaskCustomControl)),
            CancelCommandProperty = DependencyProperty.Register(nameof(CancelCommand), typeof(ICommand), typeof(NetworkTaskCustomControl));

        public event RoutedEventHandler?
            NetworkDirectionChanged,
            DoneSizeChanged,
            TotalSizeChanged,
            PercentageChanged,
            FileNameChanged,
            CancelCommandChanged;

        public static readonly RoutedEvent
            NetworkDirectionChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(NetworkDirection), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(NetworkTaskCustomControl)),

            DoneSizeChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(DoneSize), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(NetworkTaskCustomControl)),

            TotalSizeChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(TotalSize), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(NetworkTaskCustomControl)),

            PercentageChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(Percentage), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(NetworkTaskCustomControl)),

            FileNameChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(FileName), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(NetworkTaskCustomControl)),

            CancelCommandChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(CancelCommand), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(NetworkTaskCustomControl));

        public NetworkDirection NetworkDirection
        {
            get => (NetworkDirection)GetValue(NetworkDirectionProperty);
            set => SetValue(NetworkDirectionProperty, value);
        }

        public MemoryAmount DoneSize
        {
            get => (MemoryAmount)GetValue(DoneSizeProperty);
            set => SetValue(DoneSizeProperty, value);
        }

        public MemoryAmount TotalSize
        {
            get => (MemoryAmount)GetValue(TotalSizeProperty);
            set => SetValue(TotalSizeProperty, value);
        }

        public string FileName
        {
            get => (string)GetValue(FileNameProperty);
            set => SetValue(FileNameProperty, value);
        }

        public double Percentage
        {
            get => (double)GetValue(PercentageProperty);
            set => SetValue(PercentageProperty, value);
        }

        public ICommand CancelCommand
        {
            get => (ICommand)GetValue(CancelCommandProperty);
            set => SetValue(CancelCommandProperty, value);
        }
    }
}
