﻿using System.Windows;
using System.Windows.Controls;

namespace DU7
{
    public class AvailableFileCustomControl : Button
    {
        static AvailableFileCustomControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AvailableFileCustomControl), new FrameworkPropertyMetadata(typeof(AvailableFileCustomControl)));
        }

        public static readonly DependencyProperty
            FileNameProperty = DependencyProperty.Register(nameof(FileName), typeof(string), typeof(AvailableFileCustomControl)),
            FileSizeProperty = DependencyProperty.Register(nameof(FileSize), typeof(MemoryAmount), typeof(AvailableFileCustomControl));

        public event RoutedEventHandler?
            FileNameChanged,
            FileSizeChanged;

        public static readonly RoutedEvent
            FileNameChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(FileNameChanged), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AvailableFileCustomControl)),

            FileSizeChangedEvent =
                EventManager.RegisterRoutedEvent(nameof(FileSizeChanged), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AvailableFileCustomControl));

        public string FileName
        {
            get => (string)GetValue(FileNameProperty);
            set => SetValue(FileNameProperty, value);
        }

        public MemoryAmount FileSize
        {
            get => (MemoryAmount)GetValue(FileSizeProperty);
            set => SetValue(FileSizeProperty, value);
        }
    }
}
