﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace DU7
{
    public readonly struct MemoryAmount : IEquatable<MemoryAmount>, IComparable<MemoryAmount>, ISpanFormattable
    {
        private readonly ulong byteCount;

        public MemoryAmount(ulong byteCount)
        {
            this.byteCount = byteCount;
        }

        private const int bitsInUlong = 64;

        private const int kB_bytes_log2 = 10;
        private const int MB_bytes_log2 = 20;
        private const int GB_bytes_log2 = 30;
        private const int TB_bytes_log2 = 40;

        private const ulong kB_bytes = 1ul << kB_bytes_log2;
        private const ulong MB_bytes = 1ul << MB_bytes_log2;
        private const ulong GB_bytes = 1ul << GB_bytes_log2;
        private const ulong TB_bytes = 1ul << TB_bytes_log2;

        public static MemoryAmount KB => new MemoryAmount(kB_bytes);
        public static MemoryAmount MB => new MemoryAmount(MB_bytes);
        public static MemoryAmount GB => new MemoryAmount(GB_bytes);
        public static MemoryAmount TB => new MemoryAmount(TB_bytes);


        public readonly ulong TerraBytes => byteCount / TB_bytes;
        public readonly ulong GigaBytes => byteCount / GB_bytes;
        public readonly ulong MegaBytes => byteCount / MB_bytes;
        public readonly ulong KiloBytes => byteCount / kB_bytes;
        public readonly ulong Bytes => byteCount;

        public int CompareTo(MemoryAmount other)
        {
            bool thisLesser = this < other;
            bool thisGreater = this > other;
            return Unsafe.As<bool, byte>(ref thisGreater) - Unsafe.As<bool, byte>(ref thisLesser);
        }

        public bool Equals(MemoryAmount other) => this == other;

        public override bool Equals(object? obj) => obj is MemoryAmount other && this == other;

        public override int GetHashCode() => byteCount.GetHashCode();

        public bool TryFormat(Span<char> destination, out int charsWritten, ReadOnlySpan<char> format, IFormatProvider? provider)
        {
            int lastSetBitIndex = bitsInUlong - BitOperations.LeadingZeroCount(byteCount);

            return lastSetBitIndex switch 
            {
                <= bitsInUlong and >= TB_bytes_log2 
                    => destination.TryWrite(provider, $"{(float)byteCount / TB_bytes:0.000}TB", out charsWritten),

                < TB_bytes_log2 and >= GB_bytes_log2 
                    => destination.TryWrite(provider, $"{(float)byteCount / GB_bytes:0.00}GB", out charsWritten),

                < GB_bytes_log2 and >= MB_bytes_log2 
                    => destination.TryWrite(provider, $"{(float)byteCount / MB_bytes:0.0}MB", out charsWritten),

                < MB_bytes_log2 and >= kB_bytes_log2 
                    => destination.TryWrite(provider, $"{(float)byteCount / kB_bytes:0.0}kB", out charsWritten),

                _ => destination.TryWrite(provider, $"{byteCount}B", out charsWritten),
            };
        }

        [SkipLocalsInit]
        public string ToString(string? format, IFormatProvider? formatProvider)
            => string.Create(formatProvider, stackalloc char[16], $"{this}");

        [SkipLocalsInit]
        public override string ToString()
            => string.Create(null, stackalloc char[32], $"{this}");

        #region Operators
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static MemoryAmount operator +(MemoryAmount left, MemoryAmount right) => new(left.byteCount + right.byteCount);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static MemoryAmount operator -(MemoryAmount left, MemoryAmount right) => new(left.byteCount - right.byteCount);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator ulong(MemoryAmount memSize) => memSize.byteCount;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator MemoryAmount(ulong byteCount) => new(byteCount);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator ==(MemoryAmount left, MemoryAmount right) => left.byteCount == right.byteCount;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator !=(MemoryAmount left, MemoryAmount right) => left.byteCount != right.byteCount;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator >(MemoryAmount left, MemoryAmount right) => left.byteCount > right.byteCount;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator <(MemoryAmount left, MemoryAmount right) => left.byteCount < right.byteCount;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator >=(MemoryAmount left, MemoryAmount right) => left.byteCount >= right.byteCount;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator <=(MemoryAmount left, MemoryAmount right) => left.byteCount <= right.byteCount;
        #endregion
    }
}
