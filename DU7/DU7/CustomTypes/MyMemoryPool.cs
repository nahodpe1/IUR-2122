﻿using System;
using System.Buffers;

namespace DU7.CustomTypes;

internal static class MyMemoryPool
{
    internal struct MemoryOwner : IDisposable
    {
        private byte[] rented;

        internal MemoryOwner(byte[] rented)
        {
            this.rented = rented;
        }

        internal byte[] Array => rented;
        internal Memory<byte> Memory => rented;
        internal Span<byte> Span => rented;
        public void Dispose()
        {
            ArrayPool<byte>.Shared.Return(rented);
        }
    }

    internal struct MemoryOwnerExact : IDisposable
    {
        private byte[] rented;
        private int size;

        internal MemoryOwnerExact(byte[] rented, int size)
        {
            this.rented = rented;
            this.size = size;
        }

        internal Memory<byte> Memory => rented.AsMemory(0, size);
        internal Span<byte> Span => rented.AsSpan(0, size);
        public void Dispose()
        {
            ArrayPool<byte>.Shared.Return(rented);
        }
    }

    public static MemoryOwner Rent(int minimumLength)
    {
        return new(ArrayPool<byte>.Shared.Rent(minimumLength));
    }
    public static MemoryOwnerExact RentExact(int length)
    {
        return new(ArrayPool<byte>.Shared.Rent(length), length);
    }
}
