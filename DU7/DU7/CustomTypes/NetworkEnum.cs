﻿namespace DU7.CustomTypes;

public enum NetworkEnum : byte
{
    PositiveServerResponse,
    NegativeServerResponse,

    /// <summary>
    /// name
    /// </summary>
    UserToServer_Connecting,

    /// <summary>
    /// userId, fileSize, fileName, fileData
    /// </summary>
    UserToServer_SendingFile,

    /// <summary>
    /// userId, fileId
    /// </summary>
    UserToServer_RequestingFile,

    /// <summary>
    /// name, id
    /// </summary>
    ServerToUser_NewUserData,

    /// <summary>
    /// id
    /// </summary>
    ServerToUser_UserHasDisconnected,

    /// <summary>
    /// name, size, id
    /// </summary>
    ServerToUser_SendingFileData,

    /// <summary>
    /// id
    /// </summary>
    ServerToUser_FileWasDeleted,
}
