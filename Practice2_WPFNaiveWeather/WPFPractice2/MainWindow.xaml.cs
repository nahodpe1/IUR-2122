﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFPractice2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string apiKey = "47ec5d0f853a9c3001e18ad9d4cd23a8";
        static MainWindow()
        {
            WeatherNet.ClientSettings.ApiKey = apiKey;
            WeatherNet.ClientSettings.ApiUrl = @"http://api.openweathermap.org/data/2.5";
        }
        public MainWindow()
        {
            InitializeComponent();
            ComboBox_City.SelectedIndex = 0;
        }


        CancellationTokenSource citySelectionSource;
        private async void ComboBox_City_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            citySelectionSource?.Cancel();
            citySelectionSource = new CancellationTokenSource();

            var cbox = sender as ComboBox;
            string city = (e.AddedItems[0] as ComboBoxItem).Content.ToString();

            var token = citySelectionSource.Token;
            var task = Task.Run(() =>
            {
                Thread.Sleep(5000);
                token.ThrowIfCancellationRequested();
                return WeatherNet.Clients.CurrentWeather.GetByCityName(city, "Czech Republic", "en", "metric");
            });

            try
            {
                var data = await task;
                TextBlock_Temperature.Text = $"{data.Item.Temp,5:0.00} degrees Celsius";
                TextBlock_Humidity.Text = $"{data.Item.Humidity,5:0.00} %";
            }
            catch (OperationCanceledException)
            {

            }
        }
    }
}
