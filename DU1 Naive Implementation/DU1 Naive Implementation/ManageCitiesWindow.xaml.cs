﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DU1_Naive_Implementation
{
    /// <summary>
    /// Interaction logic for ManageCitiesWindow.xaml
    /// </summary>
    public partial class ManageCitiesWindow : Window
    {
        private IEnumerable<string> cities = null;
        public IEnumerable<string> Cities => cities;

        /// <param name="defaultCities">Cities that this window should be showing by default</param>
        /// <param name="onSuccess">Action that this window will take with the cities after they've been added or removed</param>
        public ManageCitiesWindow(IEnumerable<string> defaultCities)
        {
            InitializeComponent();

            ShowInTaskbar = false;

            ListBox_Cities.Items.Clear();
            foreach (var city in defaultCities)
            {
                ListBox_Cities.Items.Add(city);
            }
        }
        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            cities = ListBox_Cities.Items.OfType<string>();
            DialogResult = true;
        }
        private void Button_AddCity_Click(object sender, RoutedEventArgs e)
        {
            NewCityWindow window = new NewCityWindow();
            if (window.ShowDialog() ?? false)
            {
                ListBox_Cities.Items.Add(window.City);
            }
        }
        private void Button_RemoveCity_Click(object sender, RoutedEventArgs e)
        {
            if (ListBox_Cities.SelectedItem is not null)
            {
                var index = ListBox_Cities.SelectedIndex;
                ListBox_Cities.Items.RemoveAt(index);
                ListBox_Cities.SelectedIndex = Math.Min(ListBox_Cities.Items.Count - 1, index);
            }
            else
            {
                MessageBox.Show(this, "No selected item!", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
