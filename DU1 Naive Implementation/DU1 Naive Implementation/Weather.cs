﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WeatherNet.Clients;
using WeatherNet.Model;

namespace DU1_Naive_Implementation
{
    internal static class Weather
    {

        [Serializable]
        public class WeatherAPIException : Exception
        {
            public WeatherAPIException() { }
            public WeatherAPIException(string message) : base(message) { }
            public WeatherAPIException(string message, Exception inner) : base(message, inner) { }
            protected WeatherAPIException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
        static Weather()
        {
            WeatherNet.ClientSettings.SetApiKey(Properties.Resources.WeatherApiKey);
            WeatherNet.ClientSettings.SetApiUrl(Properties.Resources.WeatherApiUrl);
        }

        internal static Task<(double temperature, double humidity)> CzechWeatherForecastAsync(string city, DateTime day, CancellationToken token = default)
        {
            if (string.IsNullOrWhiteSpace(city))
            {
                throw new ArgumentNullException(city);
            }

            var today = DateTime.Today;
            if (day < today || day > today.AddDays(5))
            {
                throw new ArgumentException($"{nameof(day)} has to lie between today and five days later");
            }

            return Task.Run(() =>
            {
                var date = day.Date;
                if (date == today)
                {
                    token.ThrowIfCancellationRequested();
                    var result = CurrentWeather.GetByCityName(city, "Czech Republic", "en", "metric");
                    if (!result.Success)
                    {
                        throw new WeatherAPIException("The weather API returned success=false");
                    }
                    return (result.Item.Temp, result.Item.Humidity);
                }
                else
                {
                    token.ThrowIfCancellationRequested();
                    var result = FiveDaysForecast.GetByCityName(city, "Czech Republic", "en", "metric");
                    if (!result.Success)
                    {
                        throw new WeatherAPIException("The weather API returned success=false");
                    }
                    var dayForecasts =
                        result.Items
                        .SkipWhile(forecast => forecast.Date.Date != date)
                        .TakeWhile(forecast => forecast.Date.Date == date)
                        .ToList();

                    return (dayForecasts.Average(forecast => forecast.Temp), dayForecasts.Average(forecast => forecast.Humidity));
                }

            }, token);
        }
    }
}
