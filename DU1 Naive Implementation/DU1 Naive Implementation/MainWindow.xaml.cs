﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DU1_Naive_Implementation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Dictionary<TabItem, DateTime> tabItemToDays;

        private Label Label_Humidity, Label_Temperature;

        public MainWindow()
        {
            InitializeComponent();

            var today = DateTime.Today;

            tabItemToDays = new();

            for (int i = 0; i < 5; i++)
            {
                var anotherDay = today.AddDays(i);

                TabItem tab = new()
                {
                    Header = i switch
                    {
                        0 => "Current",
                        1 => "Tomorrow",
                        _ => anotherDay.ToString("dddd dd.MM.yyyy", CultureInfo.InvariantCulture)
                    },
                    ToolTip =
                        i == 0
                        ? "Current temperature and humidity"
                        : "Average temperature and humidity throughout this day"
                };

                TabControl_Days.Items.Add(tab);

                tabItemToDays[tab] = anotherDay;
            }
        }


        private void TabControlSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == TabControl_Days)
            {
                UpdateWeather();
            }
        }
        private void ComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == ComboBox_Cities)
            {
                UpdateWeather();
            }
        }

        private async IAsyncEnumerable<int> Enumerable()
        {
            yield return 10;
            yield return 10;
            yield return 10;
            yield return 10;
            yield return 10;

            await Task.Delay(100);

            yield return 20;
            yield return 20;
            yield return 20;
        }

        private async void v(IAsyncDisposable ias)
        {
            await using var IAS = ias;

            await foreach (var item in Enumerable())
            {
                Console.WriteLine(item);
            }
        }


        private CancellationTokenSource tokenSource;
        private async Task UpdateWeather()
        {
            if (!IsLoaded || ComboBox_Cities.Items.Count == 0)
            {
                return;
            }

            var tabItem = TabControl_Days.SelectedItem as TabItem;

            var selectedItem = ComboBox_Cities.SelectedItem as ComboBoxItem;
            var city = selectedItem?.Content?.ToString();
            city ??= ComboBox_Cities.SelectedItem as string;

            try
            {
                tokenSource?.Cancel();
                tokenSource = new CancellationTokenSource();

                Label_Temperature.Content = Label_Humidity.Content = "Loading...";

                var (temperature, humidity) = await Weather.CzechWeatherForecastAsync(city, tabItemToDays[tabItem], tokenSource.Token);

                Label_Temperature.Content = $"{temperature,6:0.00} °C";
                Label_Humidity.Content = $"{humidity,6:0.00} %";
            }
            catch (Exception x)
            {
                Label_Temperature.Content = Label_Humidity.Content = "Operation failed";
                if (x is not OperationCanceledException)
                {
                    MessageBox.Show(this, x.Message, "An error occured", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                }
            }
        }
        private void ButtonManageCities(object sender, RoutedEventArgs e)
        {
            ManageCitiesWindow window = new(ComboBox_Cities.Items.OfType<string>());

            if (window.ShowDialog() ?? false)
            {
                ComboBox_Cities.Items.Clear();
                foreach (var city in window.Cities)
                {
                    ComboBox_Cities.Items.Add(city);
                }
                if (ComboBox_Cities.Items.Count != 0)
                {
                    ComboBox_Cities.SelectedIndex = 0;
                }
            }
        }

        // Kod pod timto komentem slouzi pouze k ziskani referenci na label obsahující teplotu a vlhkost.
        // Nevidim lepsi zpusob, jak se odkazovat na objekty v ramci <TabControl.ContentTemplate>
        private void Label_Temperature_Loaded(object sender, RoutedEventArgs e) => Label_Temperature = (Label)sender;


        private void Label_Humidity_Loaded(object sender, RoutedEventArgs e) => Label_Humidity = (Label)sender;
    }
}
