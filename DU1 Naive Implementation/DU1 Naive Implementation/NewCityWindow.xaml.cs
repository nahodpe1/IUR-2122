﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DU1_Naive_Implementation
{
    /// <summary>
    /// Interaction logic for NewCityWindow.xaml
    /// </summary>
    public partial class NewCityWindow : Window
    {
        private string city = null;
        public string City => city;
        public NewCityWindow()
        {
            InitializeComponent();

            ShowInTaskbar = false;

            TextBox_City.Focus();
        }

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            city = TextBox_City.Text;
            DialogResult = true;
        }
    }
}
