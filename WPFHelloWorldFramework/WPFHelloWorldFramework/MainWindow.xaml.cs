﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFHelloWorldFramework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //var myGrid = new Grid();

            //var myLabel = new Label()
            //{
            //    Content = "Ahoj, jsem label z kodu.",
            //    Width = 200,
            //    Margin = new Thickness(10, 10, 0, 0)
            //};

            //var myButton = new Button()
            //{
            //    Content = "Ahoj, jsem button z kodu",
            //    Width = 100,
            //    Margin = new Thickness(10, 30, 0, 0)
            //};

            //myGrid.Children.Add(myLabel);
            //myGrid.Children.Add(myButton);

            //Content = myGrid;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            
            button.Content = $"I Was Clicked at {DateTime.Now:ss:ff}!";
            button.RenderTransform.
        }
    }
}
