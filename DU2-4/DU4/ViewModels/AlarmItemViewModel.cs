﻿using DU4.Support;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace DU4.ViewModels
{

    internal class AlarmItemViewModel : ViewModelBase
    {
        private string alarmName, message;
        private int treshold;
        private AlarmType alarmType;
        private NamedIcon selectedImage;
        public string AlarmName
        {
            get => alarmName;
            set { alarmName = value; OnPropertyChanged(); }
        }

        public string Message
        {
            get => message;
            set { message = value; OnPropertyChanged(); }
        }

        public int Treshold
        {
            get => treshold;
            set
            {
                treshold = value;
                OnPropertyChanged();
            }
        }

        public AlarmType AlarmType
        {
            get => alarmType;
            set
            {
                alarmType = value;
                OnPropertyChanged();
            }
        }

        public NamedIcon SelectedImage
        {
            get => selectedImage;
            set
            {
                selectedImage = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<NamedIcon> AlarmIcons { get; } = new ObservableCollection<NamedIcon>()
        {
            new NamedIcon("Images/cold1.png"),
            new NamedIcon("Images/cold2.png"),
            new NamedIcon("Images/cold3.png"),
            new NamedIcon("Images/hot1.png"),
            new NamedIcon("Images/hot2.png"),
            new NamedIcon("Images/hot3.png")
        };
    }

    internal enum AlarmType
    {
        MIN = 0,
        MAX = 1
    }

    public class EnumBooleanConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string parameterString = parameter as string;
            if (parameterString == null)
                return DependencyProperty.UnsetValue;

            if (!Enum.IsDefined(value.GetType(), value))
                return DependencyProperty.UnsetValue;

            object parameterValue = Enum.Parse(value.GetType(), parameterString);

            return parameterValue.Equals(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string parameterString = parameter as string;
            if (parameterString == null)
                return DependencyProperty.UnsetValue;

            return Enum.Parse(targetType, parameterString);
        }
        #endregion
    }

}