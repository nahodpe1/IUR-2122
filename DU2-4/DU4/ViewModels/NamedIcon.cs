﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DU4.ViewModels
{
    public class NamedIcon : INotifyPropertyChanged
    {
        private ImageSource icon;
        private string iconName;

        public NamedIcon(string sourceFileName)
        {
            string uriString = $"pack://application:,,,/{sourceFileName}";
            icon = new BitmapImage(new Uri(uriString));

            var fileName = uriString.Substring(uriString.LastIndexOf('/') + 1);
            var noExtension = fileName.Remove(fileName.IndexOf('.'));

            iconName = noExtension[0] + string.Join("", noExtension.Skip(1));
        }

        public ImageSource Icon
        {
            get => icon;
            set
            {
                if (icon != value)
                {
                    icon = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Icon)));
                }
            }
        }

        public string IconName
        {
            get => iconName;
            set
            {
                if (iconName != value)
                {
                    iconName = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IconName)));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
