﻿using DU4.Support;
using System.Collections.ObjectModel;

namespace DU4.ViewModels
{
    class AlarmManagerViewModel : ViewModelBase
    {
        private AlarmItemViewModel selectedAlarmDetail;
        private int selectedAlarmIndex;
        public ObservableCollection<AlarmItemViewModel> AlarmList { get; }
        public AlarmManagerViewModel()
        {
            AlarmList = new ObservableCollection<AlarmItemViewModel>()
            {
                new AlarmItemViewModel() { AlarmName = "Alarm 1" },
                new AlarmItemViewModel() { AlarmName = "Alarm 2" },
                new AlarmItemViewModel() { AlarmName = "Alarm 3" }
            };

            AddCommand = new RelayCommand(
                _ => AlarmList.Add(new AlarmItemViewModel() { AlarmName = "New Alarm" }),
                _ => AlarmList.Count < 10);

            DeleteCommand = new RelayCommand(
                _ => AlarmList.Remove(SelectedAlarmDetail),
                _ => AlarmList.Contains(SelectedAlarmDetail));
        }

        public AlarmItemViewModel SelectedAlarmDetail
        {
            get => selectedAlarmDetail;
            set
            {
                selectedAlarmDetail = value;
                OnPropertyChanged();
            }
        }
        public int SelectedAlarmIndex
        {
            get => selectedAlarmIndex;
            set
            {
                selectedAlarmIndex = value;
                OnPropertyChanged();
            }
        }
        public RelayCommand AddCommand { get; }
        public RelayCommand DeleteCommand { get; }
    }
}

