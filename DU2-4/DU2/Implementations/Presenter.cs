﻿using System;
using System.Globalization;
using System.Threading.Tasks;

namespace MVP_Example
{
    internal class Presenter : IPresenter
    {
        private IModel model;
        private IView view;
        private Task ShowNoArgumentsError() => view.WarnOfUnsucessfulOperation("No arguments were passed after the command");
        private Task ShowParseError() => view.WarnOfUnsucessfulOperation("Couldn't parse the command argument");
        private Task ShowEmptyCommandError() => view.WarnOfUnsucessfulOperation("Command was empty");
        private async Task UpdateView()
        {
            try
            {
                var weather = await model.GetWeather();

                view.City = weather.City;
                view.Country = weather.Country;
                view.Humidity = weather.Humidity;
                view.Temperature = weather.Temp;
                view.TempMax = weather.TempMax;
                view.TempMin = weather.TempMin;
                view.Description = weather.Description;

                await view.Render();
            }
            catch (Exception e)
            {
                await view.WarnOfUnsucessfulOperation("An exception occured: " + e.Message);
            }
        }
        public Presenter(IModel model, IView view)
        {
            this.model = model ?? throw new ArgumentNullException(nameof(model));
            this.view = view ?? throw new ArgumentNullException(nameof(view));
        }
        public async Task ParseCommand(string command)
        {
            const string SetLanguageCommand = "setLanguage";
            const string SetUnitsCommand = "setUnits";

            command = command.Trim();

            if (string.IsNullOrEmpty(command))
            {
                await ShowEmptyCommandError();
            }
            else if (command == "x")
            {
                Environment.Exit(0);
            }
            else if (command.StartsWith(SetLanguageCommand, StringComparison.InvariantCultureIgnoreCase))
            {
                if (command.Length <= SetLanguageCommand.Length + 1)
                {
                    await ShowNoArgumentsError();
                    return;
                }

                var languageName = command[(SetLanguageCommand.Length + 1)..];

                CultureInfo cInfo;
                try { cInfo = CultureInfo.GetCultureInfo(languageName); }
                catch
                {
                    await ShowParseError();
                    return;
                }


                model.Language = cInfo.TwoLetterISOLanguageName;
                view.Culture = cInfo;
            }
            else if (command.StartsWith(SetUnitsCommand, StringComparison.InvariantCultureIgnoreCase))
            {
                if (command.Length <= SetUnitsCommand.Length + 1)
                {
                    await ShowNoArgumentsError();
                    return;
                }

                if (Enum.TryParse(command[(SetUnitsCommand.Length + 1)..], ignoreCase: true, out UnitSystem units))
                {
                    model.UnitSystem = view.UnitSystem = units;
                }
                else await ShowParseError();
            }
            else await UpdateView();
        }
    }
}