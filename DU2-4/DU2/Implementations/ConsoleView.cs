﻿using DU2;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace MVP_Example
{
    internal class ConsoleView : IView
    {
        public ConsoleView(string city, string country, CultureInfo culture)
        {
            City = city ?? throw new ArgumentNullException(nameof(city));
            Country = country ?? throw new ArgumentNullException(nameof(country));
            Culture = culture ?? throw new ArgumentNullException(nameof(culture));
        }
        public string? Description { get; set; }
        public string City { set; get; }
        public string Country { set; get; }
        public double Temperature { set; get; }
        public double TempMax { set; get; }
        public double TempMin { set; get; }
        public double Humidity { set; get; }
        public UnitSystem UnitSystem { private get; set; }
        public CultureInfo Culture { get; set; }

        /// <returns>User command, or <see langword="null"/> if no more commands are available</returns>
        public Task<string?> GetUserInput()
        {
            Console.Write("Command for presenter (x to exit): ");
            return Task.FromResult(Console.ReadLine());
        }

        public Task Render()
        {
            var translation = LanguageParser.GetTranslations(Culture.TwoLetterISOLanguageName);

            FormattableString fs =
$@"
{DateTime.Now:F} :
{{
{translation.City} = {City}
{translation.Country} = {Country}
{translation.Description} = ""{Description ?? "None"}""
{translation.Temperature} = {Temperature:0.00} {UnitSystem.TemperatureUnit()}
{translation.TempMax} = {TempMax:0.00} {UnitSystem.TemperatureUnit()}
{translation.TempMin} = {TempMin:0.00} {UnitSystem.TemperatureUnit()}
{translation.Humidity} = {Humidity:0.00} %
}}
";
            var previousColor = Console.ForegroundColor;
            try
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(fs.ToString(Culture));
                return Task.CompletedTask;
            }
            finally
            {
                Console.ForegroundColor = previousColor;
            }
        }

        public Task WarnOfUnsucessfulOperation(string? message)
        {
            const ConsoleColor errorColor = ConsoleColor.Red;
            const ConsoleColor messageColor = ConsoleColor.DarkRed;

            var previousForeground = Console.ForegroundColor;
            try
            {
                Console.ForegroundColor = errorColor;
                Console.WriteLine("Command failed.");
                if (message is not null)
                {
                    Console.Write("Message: \"");
                    Console.ForegroundColor = messageColor;
                    Console.Write(message);
                    Console.ForegroundColor = errorColor;
                    Console.WriteLine("\"");
                }
                return Task.CompletedTask;
            }
            finally
            {
                Console.ForegroundColor = previousForeground;
            }
        }
    }
}