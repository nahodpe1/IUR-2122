﻿using System;
using System.Threading.Tasks;
using WeatherNet.Model;

namespace MVP_Example
{
    public class Model : IModel
    {
        public Model(string city, string language)
        {
            City = city ?? throw new ArgumentNullException(nameof(city));
            Language = language ?? throw new ArgumentNullException(nameof(language));
        }

        public string City { get; set; }
        public string Language { get; set; }
        public UnitSystem UnitSystem { get; set; }

        public Task<WeatherResult> GetWeather()
            => Task.Run<WeatherResult>(() =>
            {
                var result = WeatherNet.Clients.CurrentWeather.GetByCityName(City, "Czech Republic", Language, UnitSystem.ToString());

                return result.Success
                    ? result.Item ?? throw new NullReferenceException($"The {nameof(WeatherResult)} object returned from Weather API was null")
                    : throw new WeatherAPIException(result.Message);
            });


        [Serializable]
        public class WeatherAPIException : Exception
        {
            public WeatherAPIException() { }
            public WeatherAPIException(string message) : base(message) { }
            public WeatherAPIException(string message, Exception inner) : base(message, inner) { }
            protected WeatherAPIException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }
    }
}
