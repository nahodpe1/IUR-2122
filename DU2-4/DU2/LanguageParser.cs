﻿using DU2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json;

namespace DU2
{
    public static class LanguageParser
    {
        public class Translations
        {
            private static PropertyInfo[] stringProperties =
                typeof(Translations)
                .GetProperties()
                .Where(p => p.PropertyType == typeof(string))
                .ToArray();

            private static string[] propertyNames = stringProperties.Select(prop => prop.Name).ToArray();

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
            private Translations() { } // this constructor is only used for static Translation FromJson(..) which handles null values
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
            public Translations(string description, string city, string country, string temperature, string tempMax, string tempMin, string humidity)
            {
                Description = description ?? throw new ArgumentNullException(nameof(description));
                City = city ?? throw new ArgumentNullException(nameof(city));
                Country = country ?? throw new ArgumentNullException(nameof(country));
                Temperature = temperature ?? throw new ArgumentNullException(nameof(temperature));
                TempMax = tempMax ?? throw new ArgumentNullException(nameof(tempMax));
                TempMin = tempMin ?? throw new ArgumentNullException(nameof(tempMin));
                Humidity = humidity ?? throw new ArgumentNullException(nameof(humidity));

                PadProperties();
            }

            private void PadProperties()
            {
                int desiredLength = stringProperties.Max(p => ((string)p.GetValue(this)!).AsSpan().Trim().Length) + 1;

                foreach (var property in stringProperties)
                {
                    var value = property.GetValue(this) as string ?? throw new NullReferenceException();
                    property.SetValue(this, value.Trim().PadLeft(desiredLength));
                }
            }

            public string Description { get; private set; }
            public string City { get; private set; }
            public string Country { get; private set; }
            public string Temperature { get; private set; }
            public string TempMax { get; private set; }
            public string TempMin { get; private set; }
            public string Humidity { get; private set; }

            public static Translations Default { get; } =
                new Translations(
                    nameof(Description),
                    nameof(City),
                    nameof(Country),
                    nameof(Temperature),
                    nameof(TempMax),
                    nameof(TempMin),
                    nameof(Humidity));

            public static Translations FromJson(JsonElement languageJsonElement)
            {
                Translations temp = new();

                var jsonTranslations =
                    languageJsonElement
                    .EnumerateObject()
                    .Where(j => j.Value.ValueKind == JsonValueKind.String) // filter out properties without string values
                    .Select(j => (English: j.Name, Translated: j.Value.GetString()!)) // select (english, translated) tuples
                    .Distinct(tuple => tuple.English, StringComparer.OrdinalIgnoreCase) // filter out properties whose names equal in case insensitive comparison
                    .Join(
                        propertyNames, // join with names of json properties..
                        tuple => tuple.English,
                        propName => propName, // comparing them with names of this object's properties
                        (tuple, propertyName) => (PropertyName: propertyName, TranslatedValue: tuple.Translated), // result is (propertyName, translated)
                        StringComparer.OrdinalIgnoreCase) // case insensitive joining
                    .ToDictionary(tuple => tuple.PropertyName, tuple => tuple.TranslatedValue); // create a dictionary

                foreach (var objectProperty in stringProperties)
                {
                    string propertyName = objectProperty.Name;

                    objectProperty.SetValue(temp,
                        jsonTranslations.TryGetValue(propertyName, out var translation)
                        ? translation
                        : propertyName);
                }
                temp.PadProperties();
                return temp;
            }
            static Translations()
            {
                using var json = JsonDocument.Parse(Resources.languages);
                var languages = json.RootElement;

                foreach (var language in languages.GetProperty("languages").EnumerateArray())
                {
                    if (
                        language.TryGetProperty("languageCodes", out var langcodes) &&
                        langcodes.ValueKind == JsonValueKind.Array &&
                        langcodes.EnumerateArray().All(j => j.ValueKind == JsonValueKind.String) &&
                        langcodes.EnumerateArray().Any(j => !langcodeToTranslations.ContainsKey(j.GetString()!))
                        )
                    {
                        Translations translations;
                        try
                        {
                            translations = Translations.FromJson(language);
                        }
                        catch
                        {
                            continue;
                        }

                        foreach (var langcode in language.GetProperty("languageCodes").EnumerateArray())
                        {
                            langcodeToTranslations.TryAdd(langcode.GetString()!, translations);
                        }
                    }

                }
            }
        }

        private static readonly Dictionary<string, Translations> langcodeToTranslations = new();
        public static Translations GetTranslations(string languageCode)
            =>
            langcodeToTranslations.TryGetValue(languageCode, out var value)
            ? value
            : Translations.Default;
    }
}
