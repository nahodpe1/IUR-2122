﻿using System.Globalization;
using System.Threading.Tasks;

namespace MVP_Example
{
    public interface IView
    {
        string City { set; }
        string Country { set; }
        string Description { set; }
        double Temperature { set; }
        double TempMax { set; }
        double TempMin { set; }
        double Humidity { set; }
        CultureInfo Culture { set; }
        UnitSystem UnitSystem { set; }

        Task Render();
        Task<string?> GetUserInput();
        Task WarnOfUnsucessfulOperation(string? message);
    }
}