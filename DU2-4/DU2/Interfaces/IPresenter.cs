﻿using System.Threading.Tasks;

namespace MVP_Example
{
    interface IPresenter
    {
        Task ParseCommand(string command);
    }
}
