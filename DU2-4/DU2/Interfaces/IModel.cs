﻿using System.Threading.Tasks;
using WeatherNet.Model;

namespace MVP_Example
{
    interface IModel
    {
        string City { set; }
        string Language { set; }
        UnitSystem UnitSystem { set; }
        Task<WeatherResult> GetWeather();
    }
}
