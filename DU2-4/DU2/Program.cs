﻿using DU2;
using DU2.Properties;
using System.Globalization;
using System.Threading.Tasks;

namespace MVP_Example
{
    class Program
    {
        static async Task Main(string[] args)
        {
            IModel model = new Model(Resources.DefaultCity, Resources.DefaultLanguage);
            IView view = new ConsoleView(Resources.DefaultCity, "Czech Republic", CultureInfo.CurrentCulture);
            IPresenter presenter = new Presenter(model, view);

            WeatherNet.ClientSettings.ApiKey = Resources.APIKey;
            WeatherNet.ClientSettings.ApiUrl = Resources.APIUrl;

            while (true)
            {
                string? command = await view.GetUserInput();
                if (command is null)
                {
                    return;
                }

                await presenter.ParseCommand(command);
            }
        }
    }
}
