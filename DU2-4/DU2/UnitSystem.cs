﻿using System;

namespace MVP_Example
{
    public enum UnitSystem { Metric, Imperial }
    public static class UnitsExtensions
    {
        public static string TemperatureUnit(this UnitSystem unit)
            => unit switch
            {
                UnitSystem.Metric => "°C",
                UnitSystem.Imperial => "F",
                _ => throw new ArgumentException(nameof(unit))
            };
    }
}