﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace DU2
{
    public static class IEnumerableExensions
    {
        private class EqualityComparerWithSelector<TInput, TValue> : IEqualityComparer<TInput>
        {
            private Func<TInput?, TValue?> selector;
            private IEqualityComparer<TValue> innerComparer;

            public EqualityComparerWithSelector(Func<TInput?, TValue?> selector, IEqualityComparer<TValue>? comparer)
            {
                this.selector = selector ?? throw new ArgumentNullException(nameof(selector));
                innerComparer = comparer ?? EqualityComparer<TValue>.Default;
            }

            public bool Equals(TInput? x, TInput? y) => innerComparer.Equals(selector(x), selector(y));

            public int GetHashCode([DisallowNull] TInput obj) => selector(obj)?.GetHashCode() ?? 0;
        }

        /// <summary>
        /// This versions of Distinct() operates on values returned from the selector
        /// </summary>
        /// <typeparam name="TInput">The input type</typeparam>
        /// <typeparam name="TValue">The type which will be checked for equality</typeparam>
        /// <param name="selector">Selector which selects the value to compare from input</param>
        /// <param name="comparer">Optional custom comparer of selected values</param>
        /// <returns></returns>
        public static IEnumerable<TInput> Distinct<TInput, TValue>(this IEnumerable<TInput> source, Func<TInput?, TValue?> selector, IEqualityComparer<TValue>? comparer = null)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (selector is null)
            {
                throw new ArgumentNullException(nameof(selector));
            }

            comparer ??= EqualityComparer<TValue>.Default;

            return source.Distinct(new EqualityComparerWithSelector<TInput, TValue>(selector, comparer));
        }
    }
}
