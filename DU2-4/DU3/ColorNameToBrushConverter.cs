﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace DU3
{
    public class ColorNameToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!targetType.IsAssignableFrom(typeof(SolidColorBrush)))
            {
                throw new NotImplementedException($"This converter only converts color names to {nameof(SolidColorBrush)}");
            }

            string colorName = value switch
            {
                string s => s,
                ComboBoxItem c => (string)c.Content,
                _ => throw new NotImplementedException(
                    "This convertor only works with string values or with ComboBoxItem values containing strings")
            };

            return typeof(Brushes)
                .GetProperty(
                    name: colorName,
                    bindingAttr: BindingFlags.Public | BindingFlags.Static,
                    binder: null,
                    returnType: typeof(SolidColorBrush),
                    types: Array.Empty<Type>(),
                    modifiers: null)
                ?.GetValue(null)
                as SolidColorBrush
                ?? throw new ArgumentException(@$"Brush ""{colorName}"" not found");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
