﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace DU3
{
    internal class GenderNameToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string stringValue = value switch
            {
                string s => s,
                ComboBoxItem c => (string)c.Content,
                _ => throw new ArgumentException("This converter only converts from strings or ComboBoxItems with string contents")
            };

            if (!targetType.IsAssignableFrom(typeof(BitmapImage)))
            {
                throw new ArgumentException("This Converter only converts to image source");
            }

            return new BitmapImage(
                new Uri(stringValue switch
                {
                    "Muž" => "pack://application:,,,/Images/men.png",
                    "Žena" => "pack://application:,,,/Images/women.png",
                    _ => throw new ArgumentException($@"Unknown argument""{stringValue}""")
                }));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
