﻿using System.ComponentModel;
using System.Windows.Media;

namespace DU3
{
    public class NamedImage : INotifyPropertyChanged
    {
        private string? name;
        private ImageSource? imageSource;

        public string? Name
        {
            get => name;
            set
            {
                if (name != value)
                {
                    name = value;
                    PropertyChanged?.Invoke(this, new(nameof(Name)));
                }
            }
        }

        public ImageSource? ImageSource
        {
            get => imageSource;
            set
            {
                if (imageSource != value)
                {
                    imageSource = value;
                    PropertyChanged?.Invoke(this, new(nameof(ImageSource)));
                }
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;

        public override string ToString() => Name ?? "";
    }
}
