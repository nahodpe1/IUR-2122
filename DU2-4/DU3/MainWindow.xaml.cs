﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;

namespace DU3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private string name, surname, interest, email;

        public ObservableCollection<string> Interests { get; }
        public string UserName
        {
            get => name;
            set
            {
                if (name != value)
                {
                    name = value ?? throw new ArgumentException($"Attempting to set {nameof(UserName)} to null");
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UserName)));
                }
            }
        }
        public string UserSurname
        {
            get => surname;
            set
            {
                if (surname != value)
                {
                    surname = value ?? throw new ArgumentException($"Attempting to set {nameof(UserSurname)} to null");
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UserSurname)));
                }
            }
        }
        public string Interest
        {
            get => interest;
            set
            {
                if (interest != value)
                {
                    interest = value ?? throw new ArgumentException($"Attempting to set {nameof(Interest)} to null");
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Interest)));
                }
            }
        }
        public string Email
        {
            get => email;
            set
            {
                if (email != value)
                {
                    email = value ?? throw new ArgumentException($"Attempting to set {nameof(Email)} to null");
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Email)));
                }
            }
        }

        public MainWindow()
        {
            Interests = new();
            name = surname = interest = email = "";

            InitializeComponent();
        }
        public event PropertyChangedEventHandler? PropertyChanged;
        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Interest))
            {
                Interests.Add(Interest);
                Interest = "";
            }
            e.Handled = true;
        }
        private void Button_Remove_Click(object sender, RoutedEventArgs e)
        {
            if (Listbox_Interests is
                {
                    Items: { Count: > 0 },
                    SelectedIndex: not -1
                })
            {
                int index = Listbox_Interests.SelectedIndex;

                Interests.RemoveAt(index);
                Listbox_Interests.SelectedIndex = Math.Min(Listbox_Interests.Items.Count - 1, index);
                Listbox_Interests.Focus();
            }

            e.Handled = true;
        }
    }
}
